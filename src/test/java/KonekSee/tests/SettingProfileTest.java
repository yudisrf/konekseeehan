package KonekSee.tests;

import KonekSee.enums.Username;
import KonekSee.screens.RegisterProfileScreen;
import KonekSee.testrail.APIException;
import org.testng.annotations.Test;

/**
 * Created by raihan on 2/22/16.
 */
public class SettingProfileTest extends MyProfileDetailTest {
    RegisterProfileScreen profile;
    String folderName = "Setting Profile";

    @Test
    public void testDisplaySettingProfile() throws Exception {
        try {
            testId = "38087";
            profile = new RegisterProfileScreen(driver);
            profile.verifyProfileSetupText();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testEditProfPicGallery() throws Exception {
        try {
            testId = "38088";
            profile = new RegisterProfileScreen(driver);
            profile.setProfPicGallery();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testSetProfPicCamera() throws Exception {
        try {
            testId = "38088";
            profile = new RegisterProfileScreen(driver);
            profile.setProfPicCamera();
            captureScreen(folderName,testId + "_1");
            profile.retryCaptureCamera();
            captureScreen(folderName,testId + "_2");
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testSetName() throws Exception {
        try {
            testId = "38089";
            profile = new RegisterProfileScreen(driver);
            profile.setUsername(Username.ALPHA);
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testSaveBoth() throws Exception {
        try {
            testId = "38092";
            profile = new RegisterProfileScreen(driver);
            profile.saveProfile();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testSettingProfile() throws Exception {
        testDisplaySideBarMenu();
        testSelectUserAvatar();
        testSelectEditProfile();
        testDisplaySettingProfile();
        testEditProfPicGallery();
        testSetProfPicCamera();
        testSetName();
        testSaveBoth();
    }
}
