package KonekSee.tests;

import KonekSee.screens.ProfileDetailScreen;
import KonekSee.testrail.APIException;
import org.testng.annotations.Test;

/**
 * Created by raihan on 2/17/16.
 */
public class MyProfileDetailTest extends HomeTest {
    ProfileDetailScreen profileDetail;
    String folderName = "My Profile";

    @Test
    public void testDisplayProfileDetail() throws Exception {
        try {
            testId = "38098";
            captureScreen(folderName, testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testSelectEditProfile() throws Exception {
        try {
            testId = "38099";
            profileDetail = new ProfileDetailScreen(driver);
            profileDetail.selectEditProfile();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testSelectDeleteAccount() throws Exception {
        try {
            testId = "38100";
            profileDetail = new ProfileDetailScreen(driver);
            profileDetail.deleteAccount();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }
}
