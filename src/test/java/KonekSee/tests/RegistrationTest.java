package KonekSee.tests;

import KonekSee.enums.Registration;
import KonekSee.screens.RegistrationScreen;
import KonekSee.testrail.APIException;
import org.testng.annotations.Test;

/**
 * Created by raihan on 1/28/16.
 */
public class RegistrationTest extends StartedTest{
    RegistrationScreen registration;
    String folderName = "Registration";

    @Test
    public void testDisplayRegistrationScreen() throws Exception {
        try {
            testId = "37854";
            registration = new RegistrationScreen(driver);
            registration.verifyRegistrationScreen();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testSelectCountryCode() throws Exception {
        try {
            testId = "37858";
            registration = new RegistrationScreen(driver);
            registration.setCountry(Registration.VALID);
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testInputPhoneNumber() throws Exception {
        try {
            testId = "37859";
            registration = new RegistrationScreen(driver);
            registration.setPhoneNumber(Registration.VALID);
            captureScreen(folderName,testId2);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testRegisValid() throws Exception {
        try {
            testId = "37858";
            testId2 = "37859";
            testId3 = "37860";
            registration = new RegistrationScreen(driver);
            registration.setCountry(Registration.VALID);
            captureScreen(folderName,testId);
            registration.setPhoneNumber(Registration.VALID);
            captureScreen(folderName,testId2);
            registration.tapRegister();
            captureScreen(folderName,testId3);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
            testrail.sendTestRail(status, result, runId, testId2);
            testrail.sendTestRail(status, result, runId, testId3);
        }
    }


    // other tests to help scenario run smoothly
    @Test
    public void testRegisValidOtherNumber() throws Exception {
        registration = new RegistrationScreen(driver);
        registration.register(Registration.OTHER);
    }

    @Test
    public void testFakeRegis() throws Exception {
        testGettingStarted();
        registration = new RegistrationScreen(driver);
        registration.register(Registration.FAKE);
        System.out.println("Fake Register success");
    }
}