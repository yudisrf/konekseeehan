package KonekSee.tests;

import KonekSee.Abstract;
import KonekSee.screens.HomeScreen;
import KonekSee.testrail.APIException;
import org.testng.annotations.Test;

/**
 * Created by raihan on 2/17/16.
 */
public class HomeTest extends Abstract {
    HomeScreen home;
    String folderName = "Home";

    @Test
    public void testDisplayListView() throws Exception {
        try {
            testId = "38047";
            testId2 = "37888";
            home = new HomeScreen(driver);
            captureScreen(folderName,testId);
            home.changeView();
            home.verifyListView();
            captureScreen(folderName,testId2 + "_1");
            home.tapContactsTab();
            home.verifyListView();
            captureScreen(folderName,testId2 + "_2");
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
            testrail.sendTestRail(status, result, runId, testId2);
        }
    }

    @Test
    public void testDisplayGridView() throws Exception {
        try {
            testId = "37889";
            home = new HomeScreen(driver);
            home.changeView();
            home.verifyGridView();
            captureScreen(folderName,testId + "_1");
            home.tapFavoriteTab();
            home.verifyGridView();
            captureScreen(folderName,testId + "_2");
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testDisplaySideBarMenu() throws Exception {
        try {
            testId = "38094";
            home = new HomeScreen(driver);
            home.displaySideBarMenu();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testSelectHome() throws Exception {
        try {
            testId = "38095";
            home = new HomeScreen(driver);
            home.selectHome();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }


    @Test
    public void testSelectBlockedList() throws Exception {
        try {
            testId = "38096";
            home = new HomeScreen(driver);
            home.selectBlockedList();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
            Thread.sleep(2000);
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testSelectUserAvatar() throws Exception {
        try {
            testId = "38097";
            home = new HomeScreen(driver);
            home.selectUserAvatar();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testTapSearch() throws Exception {
        home = new HomeScreen(driver);
        home.tapSearchIcon();
    }

    @Test
    public void testSelectContact() throws Exception {
        home = new HomeScreen(driver);
        home.tapContact();
    }

    @Test
    public void testHome() throws Exception {
        testDisplayListView();
        testDisplayGridView();
    }
}
