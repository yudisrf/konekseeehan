package KonekSee.tests;

import KonekSee.screens.DeleteAccountScreen;
import KonekSee.testrail.APIException;
import org.testng.annotations.Test;

/**
 * Created by raihan on 2/17/16.
 */
public class DeleteAccountTest extends MyProfileDetailTest {
    DeleteAccountScreen deleteAcc;
    String folderName = "Delete Account";

    @Test
    public void testDisplayDeleteMyAccount() throws Exception {
        try {
            testId = "37559";
            deleteAcc = new DeleteAccountScreen(driver);
            deleteAcc.verifyDeleteScreenFirstText();
            deleteAcc.verifyDeleteScreenSecondText();
            deleteAcc.verifyDeleteScreenThirdtText();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testDeleteAccount() throws Exception {
        try {
            testId = "37561";
            testId2 = "37562";
            deleteAcc = new DeleteAccountScreen(driver);
            deleteAcc.confirmPhoneNumber("+1234567890");
            captureScreen(folderName,testId + "_1");
            deleteAcc.tapDelete();
            deleteAcc.verifyDeleteScreenThirdTextB();
            deleteAcc.confirmPhoneNumber("08973352739");
            captureScreen(folderName,testId + "_2");
            deleteAcc.tapDelete();
            captureScreen(folderName,testId2 + "_1");
            result = "passed";
            status = 1;
            Thread.sleep(7000);
            captureScreen(folderName,testId + "_2");
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
            testrail.sendTestRail(status, result, runId, testId2);
        }
    }

    @Test
    public void testDeleteMyAccount() throws Exception {
        testDisplaySideBarMenu();
        testSelectUserAvatar();
        testSelectDeleteAccount();
        testDisplayDeleteMyAccount();
        testDeleteAccount();
    }
}