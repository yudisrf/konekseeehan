package KonekSee.tests;

import KonekSee.screens.VerificationScreen;
import KonekSee.testrail.APIException;
import org.testng.annotations.Test;

/**
 * Created by raihan on 2/3/16.
 */
public class VerificationTest extends RegistrationTest {
    VerificationScreen verification;
    String folderName = "Verification";

    @Test
    public void testDisplayVerificationScreen() throws Exception {
        try {
            testId = "37862";
            verification = new VerificationScreen(driver);
            verification.verifyVerificationScreen();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        } catch (Exception e) {
            e.printStackTrace();
            result = "failed";
            status = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testInputInvalidLessDigit() throws Exception {
        try {
            testId = "38176";
            verification = new VerificationScreen(driver);
            verification.enterCode("1");
            captureScreen(folderName,testId + "_1");
            verification.enterCode("111");
            captureScreen(folderName,testId + "_2");
            result = "passed";
            status = 1;
        } catch (Exception e) {
            e.printStackTrace();
            result = "failed";
            status = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testInputInvalidCode() throws Exception {
        try {
            testId = "38175";
            verification = new VerificationScreen(driver);
            verification.enterCode("1111");
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        } catch (Exception e) {
            e.printStackTrace();
            result = "failed";
            status = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testResendCode() throws Exception {
        try {
            testId = "37866";
            verification = new VerificationScreen(driver);
            verification.resendCode();
            captureScreen(folderName,testId);
            verification.verifyResendSnackbar();
            result = "passed";
            status = 1;
        } catch (Exception e) {
            e.printStackTrace();
            result = "failed";
            status = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testChangeNumber() throws Exception {
        try {
            testId = "37863";
            verification = new VerificationScreen(driver);
            verification.tapChangeNumber();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        } catch (Exception e) {
            e.printStackTrace();
            result = "failed";
            status = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testInputValidCode() throws Exception {
        try {
            testId = "37861";
            testId2 = "37864";
            verification = new VerificationScreen(driver);
            verification.waitVerificationCode();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        } catch (Exception e) {
            e.printStackTrace();
            result = "failed";
            status = 5;
        } finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
            testrail.sendTestRail(status, result, runId, testId2);
        }
    }
}
