package KonekSee.tests;

import KonekSee.screens.ContactDetailScreen;
import KonekSee.testrail.APIException;
import org.testng.annotations.Test;

/**
 * Created by raihan on 2/17/16.
 */
public class ContactDetailTest extends HomeTest {
    ContactDetailScreen detail;
    String folderName = "Contact Detail";


    @Test
    public void testCloseContactDetail() throws Exception {
        try {
            testId = "38072";
            detail = new ContactDetailScreen(driver);
            detail.closeContactDetail();
            captureScreen(folderName, testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testCallNumber() throws Exception {
        try {
            testId = "38069";
            testId2 = "38063";
            detail = new ContactDetailScreen(driver);
            captureScreen(folderName,testId2);
            detail.callContact();
            captureScreen(folderName, testId);
            detail.endCall();
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
            testrail.sendTestRail(status, result, runId, testId2);
        }
    }

    @Test
    public void testBlockContact() throws Exception {
        try {
            testId = "38070";
            detail = new ContactDetailScreen(driver);
            detail.blockContact();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testInviteContact() throws Exception {
        try {
            testId = "38071";
            detail = new ContactDetailScreen(driver);
            detail.inviteContact("fadlin");
            captureScreen(folderName,testId + "_1");
            detail.verifyInvitationText();
            captureScreen(folderName,testId + "_2");
            detail.sendInvitation();
            captureScreen(folderName,testId + "_3");
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testUserDetail() throws Exception {
        testSelectContact();
        testBlockContact();
        testSelectContact();
        testCallNumber();
        testCloseContactDetail();
    }
}
