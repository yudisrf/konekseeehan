package KonekSee.tests;

import KonekSee.Abstract;
import KonekSee.screens.StartedScreen;
import KonekSee.testrail.APIException;
import org.testng.annotations.Test;

/**
 * Created by raihan on 2/5/16.
 */
public class StartedTest extends Abstract {
    StartedScreen started;
    String folderName = "Started";

    @Test
    public void testSplashScreen() throws Exception {
        try{
            testId = "37852";
//            started = new StartedScreen(driver);
            started.verifyPageTitleAndDesc("Connect.","Keep in touch with your contacts");
            captureScreen(folderName,testId + "_1");
            System.out.println("Splash screen success");
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testGettingStarted() throws Exception {
        try{
            testId = "37853";
//            started = new StartedScreen(driver);
            swipeRightToLeft(driver.findElement(map.getLocator("welcome")));
            captureScreen(folderName,testId + "_2");
            started.verifyPageTitleAndDesc("Share.","Share anything to anyone in your contacts");
            swipeRightToLeft(driver.findElement(map.getLocator("welcome")));
            captureScreen(folderName,testId + "_3");
            started.verifyPageTitleAndDesc("Enjoy.","Let's give it a try");
            started.tapJoin();
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally{
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }
}
