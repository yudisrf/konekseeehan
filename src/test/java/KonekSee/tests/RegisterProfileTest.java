package KonekSee.tests;

import KonekSee.enums.Username;
import KonekSee.screens.RegisterProfileScreen;
import KonekSee.testrail.APIException;
import org.testng.annotations.Test;

/**
 * Created by raihan on 2/2/16.
 */
public class RegisterProfileTest extends VerificationTest {
    RegisterProfileScreen profile;
    String folderName = "Register Profile";

    @Test
    public void testDisplaySettingProfile() throws Exception {
        try {
            testId = "37867";
            profile = new RegisterProfileScreen(driver);
            profile.verifyProfileSetupText();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testSetProfPicCamera() throws Exception {
        try {
            testId = "37868";
            profile = new RegisterProfileScreen(driver);
            profile.setProfPicCamera();
            captureScreen(folderName,testId + "_1");
            profile.retryCaptureCamera();
            captureScreen(folderName,testId + "_2");
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testEditProfPicGallery() throws Exception {
        try {
            testId = "37869";
            profile = new RegisterProfileScreen(driver);
            profile.setProfPicGallery();
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testSetName() throws Exception {
        try {
            testId = "37870";
            testId2 = "37871";
            profile = new RegisterProfileScreen(driver);
            profile.setUsername(Username.SYMBOL);
            captureScreen(folderName,testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
            testrail.sendTestRail(status, result, runId, testId2);
        }
    }

    @Test
    public void testSaveProfile() throws Exception {
        try {
            testId = "37874";
            profile = new RegisterProfileScreen(driver);
            captureScreen(folderName,testId);
            profile.saveProfile();
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }
}
