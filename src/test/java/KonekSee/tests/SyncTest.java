package KonekSee.tests;

import KonekSee.screens.SyncScreen;
import KonekSee.testrail.APIException;
import org.testng.annotations.Test;

/**
 * Created by raihan on 2/16/16.
 */
public class SyncTest extends RegisterProfileTest {
    SyncScreen sync;
    String folderName = "Sync";

    @Test
    public void testDisplayProgressBar() throws Exception {
        try {
            testId = "38060";
            testId2 = "37875";
            testId3 = "37887";
            sync = new SyncScreen(driver);
            captureScreen(folderName,testId);
            sync.verifyProgressBar();
            result = "passed";
            status = 1;
            sync.waitForSyncing(20);
            captureScreen(folderName,testId3);
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
            testrail.sendTestRail(status, result, runId, testId2);
            testrail.sendTestRail(status, result, runId, testId3);
        }
    }

    @Test
    public void testRegisterAccountWithoutProfile() throws Exception {
        testSplashScreen();
        testGettingStarted();
        testDisplayRegistrationScreen();
        testRegisValidOtherNumber();
        testDisplayVerificationScreen();
        testInputInvalidLessDigit();
        testInputInvalidCode();
        testResendCode();
        testChangeNumber();
        testRegisValid();
        testInputValidCode();
        testDisplaySettingProfile();
        testSaveProfile();
        testDisplayProgressBar();
    }

    @Test
    public void testRegisterAccountWithProfile() throws Exception {
        testSplashScreen();
        testGettingStarted();
        testDisplayRegistrationScreen();
        testRegisValidOtherNumber();
        testDisplayVerificationScreen();
        testInputInvalidLessDigit();
        testInputInvalidCode();
        testResendCode();
        testChangeNumber();
        testRegisValid();
        testInputValidCode();
        testDisplaySettingProfile();
        testSetProfPicCamera();
        testEditProfPicGallery();
        testSetName();
        testSaveProfile();
        testDisplayProgressBar();
    }

}
