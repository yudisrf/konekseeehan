package KonekSee.tests;

import KonekSee.multiple.BaseTest;
import KonekSee.screens.StartedScreen;
import org.testng.annotations.Test;

/**
 * Created by raihan on 4/12/16.
 */
public class MultipleDeviceTest extends BaseTest{
    StartedScreen started;
    @Test
    public void testMultipleDevices() throws Exception {
        started = new StartedScreen(driver);
        swipeRightToLeft(driver.findElement(map.getLocator("welcome")));
        started.verifyPageTitleAndDesc("Share.","Share anything to anyone in your contacts");
        swipeRightToLeft(driver.findElement(map.getLocator("welcome")));
        started.verifyPageTitleAndDesc("Enjoy.","Let's give it a try");
        started.tapJoin();
    }
}