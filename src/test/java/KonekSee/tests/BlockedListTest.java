package KonekSee.tests;

import KonekSee.screens.BlockedListScreen;
import KonekSee.testrail.APIException;
import org.testng.annotations.Test;

/**
 * Created by raihan on 2/17/16.
 */
public class BlockedListTest extends HomeTest {
    BlockedListScreen blocked;
    String folderName = "Blocked List";

    @Test
    public void testDisplayBlockedList() throws Exception {
        try {
            testId = "38177";
            blocked = new BlockedListScreen(driver);
            blocked.verifyBlockText();
            captureScreen(folderName, testId);
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testUnblockContact() throws Exception {
        try {
            testId = "38178";
            blocked = new BlockedListScreen(driver);
            blocked.unblockUser();
            captureScreen(folderName, testId + "_1");
            blocked.okUnblock();
            captureScreen(folderName, testId + "_2");
            blocked.verifyUnblockSnackbar();
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testBlockedList() throws Exception {
        testDisplaySideBarMenu();
        testSelectBlockedList();
        testDisplayBlockedList();
        testUnblockContact();
    }
}
