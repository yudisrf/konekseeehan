package KonekSee.tests;

import KonekSee.UIMapping;
import KonekSee.screens.Contacts;
import KonekSee.screens.HomeScreen;
import KonekSee.testrail.APIException;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by raihan on 2/15/16.
 */
public class ContactIntegrationTest {
    AndroidDriver driver;
    UIMapping map = new UIMapping("/Users/raihan/KonekseeProject/src/test/java/KonekSee/KonekseeUI.properties");
    HomeScreen home;
    Contacts con;
    String contactPackageName = "com.android.contacts";
    String contactActivityName = "com.android.contacts.activities.PeopleActivity";
    String konekseePackage = "com.icehousecorp.koneksee.android.stage";
    String konekseeActivity = "com.icehousecorp.koneksee.android.app.welcome.view.WelcomeActivity";
    String runId = "790";
    String testId;
    String result;
    int status;

    @BeforeMethod
    public void setUp() throws Exception {
        DesiredCapabilities capabilities = DesiredCapabilities.android();
        capabilities.setCapability("appium-version", "1.4.13");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "5.0.1");
        capabilities.setCapability("deviceName", "Galaxy S5");
        capabilities.setCapability("appPackage", konekseePackage);
        capabilities.setCapability("appActivity", konekseeActivity);
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        Thread.sleep(5000);
    }

    @Test
    public void testDeleteUserThenRefresh() throws Exception {
        try {
            testId = "38053";
            driver.startActivity(contactPackageName,contactActivityName);
            con = new Contacts(driver);
            con.search("bayu seno");
            con.delete();
            driver.navigate().back();
            home = new HomeScreen(driver);
            home.tapFavoriteTab();
            home.refreshList();
            result = "passed";
            status = 1;
            Thread.sleep(30000);
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testAddUserThenRefresh() throws Exception {
        try {
            testId = "38055";
            driver.startActivity(contactPackageName,contactActivityName);
            con = new Contacts(driver);
            con.add("Putrii","+6281214858507");
            driver.navigate().back();
            driver.navigate().back();
            home = new HomeScreen(driver);
            home.tapFavoriteTab();
            home.refreshList();
            result = "passed";
            status = 1;
            Thread.sleep(30000);
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testDeleteContactThenRefresh() throws Exception {
        try {
            testId = "38053";
            driver.startActivity(contactPackageName,contactActivityName);
            con = new Contacts(driver);
            con.search("6283");
            con.delete();
            driver.navigate().back();
            home = new HomeScreen(driver);
            home.tapContactsTab();
            home.refreshList();
            result = "passed";
            status = 1;
            Thread.sleep(30000);
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testAddContactThenRefresh() throws Exception {
        try {
            testId = "38055";
            driver.startActivity(contactPackageName,contactActivityName);
            con = new Contacts(driver);
            con.add("Putri","+6281214858507");
            driver.navigate().back();
            driver.navigate().back();
            home = new HomeScreen(driver);
            home.tapContactsTab();
            home.refreshList();
            result = "passed";
            status = 1;
            Thread.sleep(30000);
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @AfterMethod
    public void tearDown() throws Exception {
        driver.navigate().back();
        driver.navigate().back();
        driver.quit();
    }
}
