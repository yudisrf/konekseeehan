package KonekSee.tests;

import KonekSee.screens.SearchScreen;
import KonekSee.testrail.APIException;
import org.testng.annotations.Test;

/**
 * Created by raihan on 2/17/16.
 */
public class SearchTest extends HomeTest {
    SearchScreen searching;
    String folderName = "Search";

    @Test
    public void testSearchUser() throws Exception {
        try {
            testId = "38061";
            searching = new SearchScreen(driver);
            searching.searchContact("b");
            searching.verifyStarIcon();
            captureScreen(folderName,testId + "_1");
            searching.selectSearchResult();
            captureScreen(folderName,testId + "_2");
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testSearchContact() throws Exception {
        try {
            testId = "38062";
            searching = new SearchScreen(driver);
            searching.searchContact("p");
            captureScreen(folderName,testId + "_1");
            searching.selectSearchResult();
            captureScreen(folderName,testId + "_2");
            result = "passed";
            status = 1;
        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
            status = 5;
        }finally {
            APIException testrail = new APIException("");
            testrail.sendTestRail(status, result, runId, testId);
        }
    }

    @Test
    public void testSearch() throws Exception {
        testTapSearch();
        testSearchUser();
        driver.navigate().back();
        testSearchContact();
    }
}
