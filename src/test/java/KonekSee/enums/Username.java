package KonekSee.enums;

/**
 * Created by raihan on 2/1/16.
 */
public enum Username {
    ALPHA("Raihan"),
    SYMBOL("R@!H4N");
    private String name;

    Username(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
