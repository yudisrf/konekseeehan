package KonekSee.enums;

/**
 * Created by raihan on 1/28/16.
 */
public enum Registration {
    FAKE("Albania","1234567890"),
    OTHER("Indonesia","085659171242"),
    VALID("Indonesia","8973352739");

    private String country;
    private String number;

    Registration(String country, String number) {
        this.country = country;
        this.number = number;
    }

    public String getCountry() {
        return this.country;
    }

    public String getNumber() {
        return this.number;
    }
}
