package KonekSee.multiple;

/**
 * Created by raihan on 4/12/16.
 */
public class AppiumManager {

    CommandPrompt cp = new CommandPrompt();
    AvailablePorts ap = new AvailablePorts();

    /**
     * start appium with default arguments
     */
    public void startDefaultAppium()throws Exception
    {
        cp.runCommand("appium --session-override");
        Thread.sleep(5000);
    }

    /**
     * start appium with auto generated ports : appium port, chrome port, and bootstap port
     */
    public String startAppium()throws Exception
    {
        // start appium server
        String port = ap.getPort();
        String chromePort = ap.getPort();
        String bootstrapPort = ap.getPort();

        String command = "appium --session-override -p "+port+" --chromedriver-port "+chromePort+" -bp "+bootstrapPort;
        System.out.println(command);
/*
        String output = cp.runCommand(command);
        System.out.println(output);

        if(output.contains("not"))
        {
            System.out.println("\nAppium is not installed");
            System.exit(0);
        }
*/
        return port;
    }

    /**
     * start appium with modified arguments : appium port, chrome port, and bootstap port as user pass port number
     * @param port port
     * @param chromePort port
     * @param bootstrapPort port
     */
    public void startAppium(String port, String chromePort, String bootstrapPort)throws Exception
    {
        String command = "appium --session-override -p "+port+" --chromedriver-port "+chromePort+" -bp "+bootstrapPort;
        System.out.println(command);
        String output = cp.runCommand(command);
        System.out.println(output);
    }

//	public static void main(String[] args) {
//
//	}

}
