package KonekSee;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by raihan on 1/28/16.
 */
public abstract class Abstract {
    protected AppiumDriver driver;
    protected WebDriver webDriver;
    protected String runId = "800";
    protected String testId;
    protected String testId2;
    protected String testId3;
    protected String result;
    protected int status;
    protected UIMapping map = new UIMapping("/Users/raihan/KonekseeProject/src/test/java/KonekSee/KonekseeUI.properties");

    // establish connection to appium server
    @BeforeMethod
    public void setUp() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("appium-version", "1.4.13");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "5.0.1");
        capabilities.setCapability("deviceName", "Galaxy S5");
        capabilities.setCapability("appPackage", "com.icehousecorp.koneksee.android.stage");
        capabilities.setCapability("appActivity", "com.icehousecorp.koneksee.android.app.welcome.view.WelcomeActivity");
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        Thread.sleep(5000);
    }

    // end session with appium server
    @AfterMethod
    public void tearDown() throws Exception {
        driver.navigate().back();
        driver.navigate().back();
        driver.quit();
    }

    // getting screencapture from device
    protected void captureScreen(String screenFolder, String name) throws Exception {
        webDriver = new Augmenter().augment(driver);
        File file = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
//        FileUtils.copyFile(file,new File("/Users/raihan/KonekseeProject/screencapture/Android B/" + "capture-automation-" + name + "_" + dateFormat.format(date) + ".jpg"));
//        FileUtils.copyFile(file,new File("/Users/raihan/KonekseeProject/screencapture/Android B/Regression - Sprint 2/" + screenFolder + "/" + name + "_automation.jpg"));
        FileUtils.copyFile(file,new File("/Users/raihan/KonekseeProject/screencapture/Android B/Regression - Sprint 3/" + screenFolder + "/" + name + "_automation.jpg"));
    }


    protected void swipeUpToDown(WebElement element){
        int startX = (int) (element.getLocation().getX() + (driver.manage().window().getSize().getWidth() * 0.5));
        int startY = (int) (element.getLocation().getY() + (element.getLocation().getY() * 0.1));
        int endY = (int) (element.getLocation().getY() + (element.getLocation().getY() * 0.1));
        driver.swipe(startX,startY,startX,endY, (int) 0.5);
    }

    protected void swipeRightToLeft(WebElement element){
        int startX = (int) (element.getSize().getWidth() * 0.9);
        int startY = (int) (element.getSize().getHeight() * 0.5);
        int endX = (int) (element.getSize().getWidth() * 0.1);
        driver.swipe(startX,startY,endX,startY, (int) 0.5);
    }

    protected void swipeLeftToRight(WebElement element){
        int startX = (int) (element.getSize().getWidth() * 0.9);
        int startY = (int) (element.getSize().getHeight() * 0.5);
        int endX = (int) (element.getSize().getWidth() * 0.1);
        driver.swipe(endX,startY,startX,startY, (int) 0.5);
    }

    protected void scrollDown(WebElement element){
        int startX = (int) (element.getSize().getWidth() * 0.5);
        int startY = (int) (element.getSize().getHeight() * 0.9);
        int endY = (int) (element.getSize().getHeight() * 0.3);
        driver.swipe(startX,startY,startX,endY, (int) 0.5);
    }
}
