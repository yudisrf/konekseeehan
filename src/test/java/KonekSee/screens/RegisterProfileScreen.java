package KonekSee.screens;

import KonekSee.Abstract;
import KonekSee.enums.Username;
import io.appium.java_client.AppiumDriver;

/**
 * Created by raihan on 2/1/16.
 */
public class RegisterProfileScreen extends Abstract{
    public RegisterProfileScreen(AppiumDriver driver){
        this.driver = driver;
    }

    private void captureCamera(){
        int x = (int) (driver.manage().window().getSize().getWidth() * 0.5);
        int y = (int) (driver.manage().window().getSize().getHeight() * 0.9);
        driver.tap(1,x,y, (int) 0.5);
    }

    private void selectAlbum(){
        int x = (int)171.5;
        int y = (int)868.3;
        driver.tap(1,x,y, (int) 0.5);
    }

    private void selectImage(){
        int x = 925;
        int y = 350;
        driver.tap(1,x,y, (int) 0.5);
    }

    public void setProfPicCamera() throws Exception {
        driver.findElement(map.getLocator("change.photo")).click();
        driver.findElement(map.getLocator("camera")).click();
        Thread.sleep(3000);
        captureCamera();
        Thread.sleep(3000);
        driver.findElement(map.getLocator("camera.ok")).click();
        driver.findElement(map.getLocator("crop.done")).click();
    }

    public void retryCaptureCamera() throws Exception {
        driver.findElement(map.getLocator("change.photo")).click();
        driver.findElement(map.getLocator("camera")).click();
        Thread.sleep(3000);
        captureCamera();
        Thread.sleep(3000);
        driver.findElement(map.getLocator("camera.retry")).click();
        captureCamera();
        Thread.sleep(3000);
        driver.findElement(map.getLocator("camera.ok")).click();
        driver.findElement(map.getLocator("crop.done")).click();
    }

    public void setProfPicGallery() throws Exception {
        driver.findElement(map.getLocator("change.photo")).click();
        driver.findElement(map.getLocator("gallery")).click();
        Thread.sleep(2000);
        selectAlbum();
        Thread.sleep(2000);
        selectImage();
        driver.findElement(map.getLocator("crop.done")).click();
    }

    public void setUsername(Username user) throws Exception{
        driver.findElement(map.getLocator("username.field")).click();
        driver.findElement(map.getLocator("username.field")).clear();
        driver.findElement(map.getLocator("username.field")).sendKeys(user.getName());
        driver.hideKeyboard();
    }

    public void saveProfile() throws Exception {
        driver.findElement(map.getLocator("save.profile.btn")).click();
    }

    public void verifyProfileSetupText() throws Exception {
        if (!driver.findElement(map.getLocator("profile.setup.text")).getText().equals("This is an optional profile photo and username, you can set it later")){
            throw new Exception("verify profile setup text failed");
        }else{
            System.out.println("verify profile setup text success");
        }
    }
}