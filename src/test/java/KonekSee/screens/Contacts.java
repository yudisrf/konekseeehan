package KonekSee.screens;

import KonekSee.Abstract;
import io.appium.java_client.AppiumDriver;

/**
 * Created by raihan on 2/16/16.
 */
public class Contacts extends Abstract {
    public Contacts(AppiumDriver driver) {
        this.driver = driver;
    }

    public void search(String contact) throws Exception {
        driver.findElement(map.getLocator("search")).click();
        driver.findElement(map.getLocator("search")).clear();
        driver.findElement(map.getLocator("search")).sendKeys(contact);
        driver.findElement(map.getLocator("contact.name")).click();
    }

    public void delete() throws Exception {
        driver.findElement(map.getLocator("humberger")).click();
        driver.findElement(map.getLocator("option.delete")).click();
        driver.findElement(map.getLocator("option.delete.ok")).click();
    }

    public void add(String contactName, String phoneNumber) throws Exception {
        driver.findElement(map.getLocator("add.contact")).click();
        putContactName(contactName);
        putPhoneNumber(phoneNumber);
        driver.findElement(map.getLocator("add.save")).click();
        Thread.sleep(2000);
    }

    private void putContactName(String contactName) throws Exception {
        driver.findElement(map.getLocator("add.name")).click();
        driver.findElement(map.getLocator("add.name")).clear();
        driver.findElement(map.getLocator("add.name")).sendKeys(contactName);
        driver.hideKeyboard();
        Thread.sleep(2000);
    }

    private void putPhoneNumber(String phoneNumber) throws Exception {
        driver.findElement(map.getLocator("add.number")).click();
        driver.findElement(map.getLocator("add.number")).clear();
        driver.findElement(map.getLocator("add.number")).sendKeys(phoneNumber);
        driver.hideKeyboard();
        Thread.sleep(2000);
    }
}
