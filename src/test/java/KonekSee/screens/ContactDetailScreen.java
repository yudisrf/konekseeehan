package KonekSee.screens;

import KonekSee.Abstract;
import io.appium.java_client.AppiumDriver;

/**
 * Created by raihan on 2/15/16.
 */
public class ContactDetailScreen extends Abstract {

    public ContactDetailScreen(AppiumDriver driver) {
        this.driver = driver;
    }

    public void closeContactDetail() throws Exception {
        driver.findElement(map.getLocator("back")).click();
    }

    public void blockContact() throws Exception {
        driver.findElement(map.getLocator("three.dots.block")).click();
        driver.findElement(map.getLocator("block.contact")).click();
        driver.findElement(map.getLocator("block.ok")).click();
    }

    public void cancelBlock() throws Exception {
        driver.findElement(map.getLocator("three.dots.block")).click();
        driver.findElement(map.getLocator("block.contact")).click();
        driver.findElement(map.getLocator("block.cancel")).click();
    }

    public void callContact() throws Exception{
        driver.findElement(map.getLocator("call.button")).click();
    }

    public void endCall() throws Exception {
        driver.findElement(map.getLocator("call.end")).click();
        Thread.sleep(3000);
        driver.navigate().back();
    }

    public void inviteContact(String name) throws Exception {
        driver.findElement(map.getLocator("search.contact")).click();
        driver.findElement(map.getLocator("search.field")).click();
        driver.findElement(map.getLocator("search.field")).clear();
        driver.findElement(map.getLocator("search.field")).sendKeys(name);
        driver.hideKeyboard();
        driver.findElement(map.getLocator("search.result.list")).click();
        driver.findElement(map.getLocator("invite.button")).click();
    }

    public void verifyInvitationText() throws Exception{
        if (driver.findElement(map.getLocator("text.invite")).getText().equals("Hi friend. Did you ever hear KonekSee? Well, it's a cool app. Join Now!")){
            System.out.println("Verify invitation text success");
        }else {
            throw new Exception("Verify invitation text failed");
        }
    }

    public void sendInvitation() throws Exception {
        driver.findElement(map.getLocator("send.invite")).click();
        driver.navigate().back();
    }
}