package KonekSee.screens;

import KonekSee.Abstract;
import io.appium.java_client.AppiumDriver;

/**
 * Created by raihan on 2/15/16.
 */
public class ProfileDetailScreen extends Abstract {
    public ProfileDetailScreen(AppiumDriver driver) {
        this.driver = driver;
    }

    public void verifyName(String name) throws Exception {
        if (driver.findElement(map.getLocator("profile.name")).getText().equals(name)){
            System.out.println("Verify profile name success");
        }else {
            throw new Exception("Verify profile name failed");
        }
    }

    public void verifyNumber(String number) throws Exception {
        if (driver.findElement(map.getLocator("profile.number")).getText().equals(number)){
            System.out.println("Verify profile number success");
        }else {
            throw new Exception("Verify profile number failed");
        }
    }

    public void selectEditProfile() throws Exception {
        driver.findElement(map.getLocator("profile.humberger")).click();
        driver.findElement(map.getLocator("profile.edit")).click();
    }

    public void deleteAccount() throws Exception {
        driver.findElement(map.getLocator("profile.humberger")).click();
        driver.findElement(map.getLocator("profile.delete")).click();
        driver.findElement(map.getLocator("profile.delete.ok")).click();
    }

    public void cancelDeleteAccount() throws Exception {
        driver.findElement(map.getLocator("profile.humberger")).click();
        driver.findElement(map.getLocator("profile.delete")).click();
        driver.findElement(map.getLocator("profile.delete.cancel")).click();
    }
}
