package KonekSee.screens;

import KonekSee.Abstract;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by raihan on 2/15/16.
 */
public class BlockedListScreen extends Abstract {
    public BlockedListScreen (AppiumDriver driver){
        this.driver = driver;
    }

    public void verifyBlockText() throws Exception {
        if (driver.findElement(map.getLocator("blocked.title")).getText().equals("Below is list of your friends that you've been blocked, you can unblock them.")){
            System.out.println("Verify blocked list screen success");
        }else {
            throw new Exception("Verify blocked list screen failed");
        }
    }

    public void unblockContact(String contactName) throws Exception {
        WebElement list = driver.findElement(map.getLocator("blocked.list.container"));
        WebElement blockedList = driver.findElement(map.getLocator("blocked.list1.username"));
        for (int i=1; i<=4; i++){
            if (!blockedList.getText().equals(contactName)){
                blockedList = driver.findElement(By.xpath("/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.support.v7.widget.RecyclerView[1]/android.widget.RelativeLayout[" + i + "]/android.widget.TextView[1]"));
                System.out.println(i + ". " + blockedList.getText());
                if (i == 4){
                    scrollDown(list);
                    i = 0;
                }
            }else{
                driver.findElement(By.xpath("/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.support.v7.widget.RecyclerView[1]/android.widget.RelativeLayout[" + i + "]/android.widget.ImageView[2]")).click();
                break;
            }
        }
    }

    public void unblockUser() throws Exception{
        driver.findElement(map.getLocator("blocked.list.unblock")).click();
    }

    public void okUnblock() throws Exception {
        driver.findElement(map.getLocator("unblock.ok")).click();
    }

    public void cancelUnblock() throws Exception {
        driver.findElement(map.getLocator("unblock.cancel")).click();
    }

    public void closeBlockedListScreen() throws Exception {
        driver.findElement(map.getLocator("back")).click();
    }

    public void verifyUnblockSnackbar() throws Exception{
        if (!driver.findElement(map.getLocator("snackbar.text")).getText().equals("Unblocked success")){
            throw new Exception("verify unblock snackbar failed");
        }else{
            System.out.println("verify unblock snackbar text success");
        }
    }
}
