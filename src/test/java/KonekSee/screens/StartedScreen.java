package KonekSee.screens;

import KonekSee.Abstract;
import KonekSee.multiple.BaseTest;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

/**
 * Created by raihan on 1/28/16.
 */
public class StartedScreen extends BaseTest{
    public StartedScreen(AndroidDriver driver)
    {
        this.driver = driver;
    }

    public void tapJoin() throws Exception {
        driver.findElement(map.getLocator("join.btn")).click();
    }

    public void verifyPageTitleAndDesc(String title, String desc) throws Exception {
        if (!(driver.findElement(map.getLocator("page.title")).getText().equals(title)) && (driver.findElement(map.getLocator("page.desc")).getText().equals(desc))){
            throw new Exception("verify page title and desc failed");
        }else{
            System.out.println("Verify " + title + " OK");
        }
    }
}