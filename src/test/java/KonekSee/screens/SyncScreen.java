package KonekSee.screens;

import KonekSee.Abstract;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by raihan on 2/1/16.
 */
public class SyncScreen extends Abstract {
    public SyncScreen(AppiumDriver driver){
        this.driver = driver;
    }

    public void waitForSyncing(long time) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver,time);
        wait.until(ExpectedConditions.visibilityOfElementLocated(map.getLocator("search.contact")));
    }

    public void verifyProgressBar() throws Exception {
        if (driver.findElement(map.getLocator("sync.bar")).isDisplayed()){
            System.out.println("Progress bar is displayed");
        }else {
            throw new Exception("Progress bar is not verified");
        }
    }


}
