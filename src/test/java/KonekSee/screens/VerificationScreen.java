package KonekSee.screens;

import KonekSee.Abstract;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by raihan on 2/1/16.
 */
public class VerificationScreen extends Abstract {
    public VerificationScreen (AppiumDriver driver){
        this.driver = driver;
    }

    public void verifyVerificationScreen() throws Exception {
        boolean text1 = driver.findElement(map.getLocator("sent.vercode.text")).getText().equals("We've already send verification code to below number");
        boolean text2 = driver.findElement(map.getLocator("enter.vercode.text")).getText().equals("Please enter 4 digits code that you receive");
        boolean text3 = driver.findElement(map.getLocator("didnt.get.code")).getText().equals("Didn't get the code ?");
        if (!(text1 && text2 && text3)){
            throw new Exception("verify verification screen failed");
        }else {
            System.out.println("Verification screen success");
        }
    }

    public void tapChangeNumber() throws Exception {
        driver.findElement(map.getLocator("change.number")).click();
    }

    public void resendCode() throws Exception {
        driver.findElement(map.getLocator("resend")).click();
    }

    public void waitVerificationCode() throws Exception {
        WebElement vercode = driver.findElement(map.getLocator("ver.code"));
        if (vercode.getText().equals("Code")){
            Thread.sleep(4000);
        }
    }

    public void enterCode(String code) throws Exception {
        WebElement vercode = driver.findElement(map.getLocator("ver.code"));
        vercode.click();
        vercode.clear();
        vercode.sendKeys(code);
        driver.hideKeyboard();
    }

    public void verifyResendSnackbar() throws Exception {
        if (!driver.findElement(map.getLocator("snackbar.text")).getText().equals("Code resent succesfully")){
            throw new Exception("verify resend snackbar failed");
        }else{
            System.out.println("verify resend snackbar text success");
        }
    }
}
