package KonekSee.screens;

import KonekSee.Abstract;
import io.appium.java_client.AppiumDriver;

/**
 * Created by raihan on 2/15/16.
 */
public class DeleteAccountScreen extends Abstract {
    public DeleteAccountScreen(AppiumDriver driver) {
        this.driver = driver;
    }

    public void verifyDeleteScreenFirstText() throws Exception {
        if (!driver.findElement(map.getLocator("delete.text1")).getText().equals("Is this goodbye?")){
            throw new Exception("verify delete text 1 failed");
        }else{
            System.out.println("verify delete text 1 success");
        }
    }

    public void verifyDeleteScreenSecondText() throws Exception {
        if (!driver.findElement(map.getLocator("delete.text2")).getText().equals("Are you sure you don't want to reconsider ?")){
            throw new Exception("verify delete text 2 failed");
        }else{
            System.out.println("verify delete text 2 success");
        }
    }

    public void verifyDeleteScreenThirdtText() throws Exception {
        if (!driver.findElement(map.getLocator("delete.text3")).getText().equals("If you don't want to change your mind, please confirm your phone number to make sure it's you")){
            throw new Exception("verify delete text 3 failed");
        }else{
            System.out.println("verify delete text 3 success");
        }
    }

    public void verifyDeleteScreenThirdTextB() throws Exception {
        if (!driver.findElement(map.getLocator("delete.text3")).getText().equals("Well, did you write the right phone number? We didn't notify this number is yours.\n" + "Check your phone number again, and retry.")){
            throw new Exception("verify delete text 3 failed");
        }else{
            System.out.println("verify delete text 3 success");
        }
    }

    public void confirmPhoneNumber(String number) throws Exception {
        driver.findElement(map.getLocator("number.field")).click();
        driver.findElement(map.getLocator("number.field")).clear();
        driver.findElement(map.getLocator("number.field")).sendKeys(number);
        driver.hideKeyboard();
    }

    public void tapDelete() throws Exception {
        driver.findElement(map.getLocator("delete.button")).click();
    }
}
