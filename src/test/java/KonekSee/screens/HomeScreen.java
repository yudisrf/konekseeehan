package KonekSee.screens;

import KonekSee.Abstract;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by raihan on 2/15/16.
 */
public class HomeScreen extends Abstract{
    public HomeScreen(AppiumDriver driver) {
        this.driver = driver;
    }

    public void tapSearchIcon() throws Exception {
        driver.findElement(map.getLocator("search.contact")).click();
    }

    public void pullToRefresh() throws Exception {
        swipeUpToDown(driver.findElement(map.getLocator("list.user.container")));
    }

    public void refreshList() throws Exception {
        driver.findElement(map.getLocator("three.dots")).click();
        driver.findElement(map.getLocator("refresh")).click();
    }

    public void changeView() throws Exception {
        driver.findElement(map.getLocator("three.dots")).click();
        driver.findElement(map.getLocator("list.view")).click();
    }

    public void verifyListView() throws Exception{
        if (driver.findElement(map.getLocator("list")).isDisplayed()){
            System.out.println("Verify list view success");
        }else {
            throw new Exception("Verify list view failed");
        }
    }

    public void verifyGridView() throws Exception{
        if (driver.findElement(map.getLocator("grid")).isDisplayed()){
            System.out.println("Verify grid view success");
        }else {
            throw new Exception("Verify grid view failed");
        }
    }

    public void tapContact() throws Exception {
        driver.findElement(map.getLocator("contact.grid")).click();
    }

    public void tapFavoriteTab() throws Exception {
        driver.findElement(map.getLocator("favorite")).click();
    }

    public void tapContactsTab() throws Exception {
        driver.findElement(map.getLocator("contacts")).click();
    }

    public void selectContact(String name) throws Exception {
        WebElement contactList = driver.findElement(map.getLocator("list.user.container"));
        WebElement contactName = driver.findElement(map.getLocator("contact"));
        for (int i=1; i<=7; i++){
            if (!contactName.getText().equals(name)){
                contactName = driver.findElement(By.xpath("//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.support.v4.view.ViewPager[1]/android.widget.RelativeLayout[1]/android.support.v7.widget.RecyclerView[1]/android.widget.LinearLayout[" + i + "]/android.widget.TextView[1]"));
                System.out.println(i + ". " + contactName.getText());
                if (i == 7){
                    scrollDown(contactList);
                    i = 0;
                }
            }else{
                contactName.click();
                break;
            }
        }
    }

    private void openSidebar(WebElement element){
        int startX = 50;
        int startY = (int) (element.getSize().getHeight() * 0.5);
        int endX = 900;
        driver.swipe(startX,startY,endX,startY, (int) 0.5);
    }

    private void closeSidebar(WebElement element){
        int startX = 900;
        int startY = (int) (element.getSize().getHeight() * 0.5);
        int endX = 50;
        driver.swipe(startX,startY,endX,startY, (int) 0.5);
    }

    public void displaySideBarMenu() throws Exception {
//        driver.findElement(map.getLocator("back")).click();
        openSidebar(driver.findElement(map.getLocator("layout")));
    }

    public void closeSideBarMenu() throws Exception{
        closeSidebar(driver.findElement(map.getLocator("layout")));
    }

    public void selectUserAvatar() throws Exception {
        driver.findElement(map.getLocator("avatar")).click();
    }

    public void selectHome() throws Exception {
        driver.findElement(map.getLocator("home")).click();
    }

    public void selectBlockedList() throws Exception {
        driver.findElement(map.getLocator("blocked.list")).click();
    }

    public String getNameSideBar() throws Exception {
        return driver.findElement(map.getLocator("side.name")).getText();
    }
}
