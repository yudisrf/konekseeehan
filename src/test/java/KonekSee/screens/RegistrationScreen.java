package KonekSee.screens;

import KonekSee.Abstract;
import KonekSee.enums.Registration;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by raihan on 1/28/16.
 */
public class RegistrationScreen extends Abstract {
    public RegistrationScreen(AppiumDriver driver) {
        this.driver = driver;
    }

    public void setCountry(Registration country) throws Exception {
        driver.findElement(map.getLocator("select.country")).click();
        WebElement list = driver.findElement(map.getLocator("country.list.container"));
        WebElement countryList = driver.findElement(map.getLocator("country1"));
        if (country.getCountry().equals("")){
            driver.navigate().back();
        }else{
            for (int i=1; i<=9; i++){
                if (!countryList.getText().equals(country.getCountry())){
                    countryList = driver.findElement(By.xpath("//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[2]/android.widget.RelativeLayout[1]/android.support.v7.widget.RecyclerView[1]/android.widget.LinearLayout[" + i + "]/android.widget.TextView[1]"));
                    System.out.println(i + ". " + countryList.getText());
                    if (i == 9){
                        scrollDown(list);
                        i = 0;
                    }
                }else{
                    countryList.click();
                    break;
                }
            }
        }
    }

    public void setPhoneNumber(Registration number) throws Exception {
        driver.findElement(map.getLocator("insert.number")).click();
        driver.findElement(map.getLocator("insert.number")).clear();
        driver.findElement(map.getLocator("insert.number")).sendKeys(number.getNumber());
        driver.hideKeyboard();
    }

    public void tapRegister() throws Exception {
        driver.findElement(map.getLocator("register.done.btn")).click();
    }

    public void register(Registration reg) throws Exception {
        setCountry(reg);
        setPhoneNumber(reg);
        tapRegister();
    }

    public void verifyInvalidSnackbar() throws Exception {
        if (!driver.findElement(map.getLocator("snackbar.text")).getText().equals("Phone number is invalid")){
            throw new Exception("verify invalid snackbar failed");
        }else{
            System.out.println("verify invalid snackbar text success");
        }
    }

    public void verifyRegistrationScreen() throws Exception {
        boolean text1 = driver.findElement(map.getLocator("greeting")).getText().equals("Hi There, \n" + "Please enter your phone number :");
        boolean text2 = driver.findElement(map.getLocator("announcement")).getText().equals("KonekSee will send you a SMS to verify your phone number");
        if (!(text1 && text2)){
            throw new Exception("verify registration screen failed");
        }else{
            System.out.println("verify registration screen success");
        }
    }
}
