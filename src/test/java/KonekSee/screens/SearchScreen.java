package KonekSee.screens;

import KonekSee.Abstract;
import io.appium.java_client.AppiumDriver;

/**
 * Created by raihan on 2/15/16.
 */
public class SearchScreen extends Abstract{
    public SearchScreen(AppiumDriver driver) {
        this.driver = driver;
    }

    public void searchContact(String name) throws Exception {
        driver.findElement(map.getLocator("search.field")).click();
        driver.findElement(map.getLocator("search.field")).clear();
        driver.findElement(map.getLocator("search.field")).sendKeys(name);
    }

    public void selectSearchResult() throws Exception {
        driver.findElement(map.getLocator("search.result.list")).click();
    }

    public void verifyStarIcon() throws Exception{
        if (driver.findElement(map.getLocator("search.star")).isDisplayed()){
            System.out.println("Verify love icon success");
        }else {
            throw new Exception("Verify love icon failed");
        }
    }
}
