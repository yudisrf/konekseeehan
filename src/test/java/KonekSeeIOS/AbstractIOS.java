package KonekSeeIOS;

import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by raihan on 3/1/16.
 */
public abstract class AbstractIOS {
    protected IOSDriver driver;
    protected UIMappingIOS map = new UIMappingIOS("/Users/raihan/KonekseeProject/src/test/java/KonekSeeIOS/KonekseeIOS.properties");

    @BeforeMethod
    public void setUp() throws Exception {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("appium-version", "1.4.13");
        caps.setCapability("platformName", "iOS");
        caps.setCapability("platformVersion", "8.2");
        caps.setCapability("bundleId", "com.icehousecorp.KonekSee");
        caps.setCapability("appName", "KonekSee");
        caps.setCapability("deviceName", "iPhone 6 Plus");
        driver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void tearDown() throws Exception {
        driver.quit();
    }

    protected void scrollDown(WebElement element){
        int startX = (int) (element.getSize().getWidth() * 0.5);
        int startY = (int) (element.getSize().getHeight() * 0.9);
        int endY = (int) (element.getSize().getHeight() * 0.2);
        driver.swipe(startX,startY,startX,endY, (int) 0.5);
    }

}
