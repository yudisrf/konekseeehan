package KonekSeeIOS.screens;

import KonekSeeIOS.AbstractIOS;
import io.appium.java_client.ios.IOSDriver;

/**
 * Created by raihan on 3/1/16.
 */
public class Verification extends AbstractIOS{
    public Verification(IOSDriver driver) {
        this.driver = driver;
    }

    public void inputCode(String code) throws Exception {
        driver.findElement(map.getLocator("verificationcode")).clear();
        driver.findElement(map.getLocator("verificationcode")).sendKeys(code);
    }

    public void freeze(long time) throws Exception{
        if (driver.findElement(map.getLocator("verificationcode")).getText().equals("Verification Code")){
            Thread.sleep(time);
        }
    }

    public void tapVerify() throws Exception {
        driver.findElement(map.getLocator("verifybutton")).click();
    }

    public void tapOkAfterWrong() throws Exception {
        driver.findElement(map.getLocator("wrong_ok")).click();
    }
}