package KonekSeeIOS.screens;

import KonekSeeIOS.AbstractIOS;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.By;

/**
 * Created by raihan on 3/2/16.
 */
public class Profile extends AbstractIOS {
    public Profile(IOSDriver driver) {
        this.driver = driver;
    }

    public void tapPictureField() throws Exception{
        driver.findElement(map.getLocator("picturefield")).click();
    }

    public void selectPicture(String albumName, int photoNumber){
        driver.findElement(By.name(albumName)).click();
        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[" + photoNumber + "]")).click();
    }

    public void croppingOk() throws Exception{
        driver.findElement(By.name("Choose")).click();
//        driver.findElement(map.getLocator("choosecrop")).click();
    }

    public void croppingCancel() throws Exception{
        driver.findElement(By.name("Cancel")).click();
//        driver.findElement(map.getLocator("cancelcrop")).click();
    }

    public void inputUsername(String username) throws Exception{
        driver.findElement(map.getLocator("usernamefield")).click();
        driver.findElement(map.getLocator("usernamefield")).clear();
        driver.findElement(map.getLocator("usernamefield")).sendKeys(username);
    }

    public void tapNext() throws Exception{
        driver.findElement(map.getLocator("next")).click();
    }
}
