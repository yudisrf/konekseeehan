package KonekSeeIOS.screens;

import KonekSeeIOS.AbstractIOS;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by raihan on 3/1/16.
 */
public class Registration extends AbstractIOS {
    public Registration(IOSDriver driver) {
        this.driver = driver;
    }

    public void tapCountry() throws Exception {
        driver.findElement(map.getLocator("countryselector")).click();
    }

    public void selectCountry(String country){
        driver.findElement(By.name(country)).click();
    }

    public void findCountry(String country) throws Exception {
        WebElement list = driver.findElement(map.getLocator("country_container"));
        WebElement countryOnLIst = driver.findElement(map.getLocator("country_afghanistan_text"));
        if (country.equals("")){
            driver.navigate().back();
        }else{
            for (int i=1; i<=300; i++){
                if (!countryOnLIst.getText().contains(country)){
                    countryOnLIst = driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[" + i + "]/UIAStaticText[1]"));
                    System.out.println(i + ". " + countryOnLIst.getText());
                    if (i % 20 == 0){
                        scrollDown(list);
                    }
                }else{
                    countryOnLIst.click();
                    break;
                }
            }
        }
    }

    public void tapDone() throws Exception {
        driver.findElement(map.getLocator("done")).click();
    }

    public void inputNumber(String phoneNumber) throws Exception {
        driver.findElement(map.getLocator("phonenumber")).click();
        driver.findElement(map.getLocator("phonenumber")).clear();
        driver.findElement(map.getLocator("phonenumber")).sendKeys(phoneNumber);
    }

    public void tickTnc() throws Exception{
        driver.findElement(map.getLocator("tnc")).click();
    }

    public void tapTnc() throws Exception{
        driver.findElement(map.getLocator("tnc_screen")).click();
    }

    public void tapRegister() throws Exception{
        driver.findElement(map.getLocator("register")).click();
    }
}
