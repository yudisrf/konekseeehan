package KonekSeeIOS.tests;

import KonekSeeIOS.AbstractIOS;
import KonekSeeIOS.screens.Registration;
import org.testng.annotations.Test;

/**
 * Created by raihan on 3/1/16.
 */
public class IOSRegistrationTest extends AbstractIOS{
    Registration reg;

    @Test
    public void testRegisterAccount() throws Exception {
        reg = new Registration(driver);
        reg.tapCountry();
        reg.selectCountry("(+62) Indonesia");
        Thread.sleep(3000);
        reg.tapDone();
        reg.inputNumber("08973352739");
        reg.tickTnc();
        reg.tapRegister();
        Thread.sleep(10000);
    }
}
