package KonekSeeIOS.tests;

import KonekSeeIOS.screens.Profile;
import org.testng.annotations.Test;

/**
 * Created by raihan on 3/2/16.
 */
public class IOSProfileSettingTest extends IOSVerificationTest {
    Profile profile;

    @Test
    public void testSelectPicture() throws Exception {
        testVerifyCorrectCode();
        profile = new Profile(driver);
        profile.tapPictureField();
        profile.selectPicture("Camera Roll", 999);
        profile.croppingOk();
    }

    @Test
    public void testSetUsername() throws Exception {
        profile = new Profile(driver);
        profile.inputUsername("mas joko ta'u'u");
    }

    @Test
    public void testTapNext() throws Exception {
        profile = new Profile(driver);
        profile.tapNext();
    }

    @Test
    public void testSaveProfile() throws Exception {
        testSelectPicture();
        testSetUsername();
        testTapNext();
    }
}
