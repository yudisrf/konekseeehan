package KonekSeeIOS.tests;

import KonekSeeIOS.screens.Verification;
import org.testng.annotations.Test;

/**
 * Created by raihan on 3/1/16.
 */
public class IOSVerificationTest extends IOSRegistrationTest {
    Verification ver;

    @Test
    public void testVerifyWrongCode() throws Exception {
        testRegisterAccount();
        ver = new Verification(driver);
        ver.inputCode("1234");
        ver.tapVerify();
        ver.tapOkAfterWrong();
    }

    @Test
    public void testVerifyCorrectCode() throws Exception {
        testRegisterAccount();
        ver = new Verification(driver);
        ver.freeze(15000);
        ver.tapVerify();
        Thread.sleep(5000);
    }
}
